import 'package:bloc_tests/models/product.dart';

abstract class ProductApi {
  Future<List<Product>> getAllProducts();
  Future<bool> addProduct(Product product);
  Future<bool> removeProduct(String productId);
}
