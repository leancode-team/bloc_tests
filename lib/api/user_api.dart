abstract class UserApi {
  Future<bool> signUp(String username, String password);
}
