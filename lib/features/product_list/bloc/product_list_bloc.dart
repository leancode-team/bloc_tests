import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:bloc_tests/api/product_api.dart';
import 'package:bloc_tests/models/product.dart';
import 'package:meta/meta.dart';
import './bloc.dart';

class ProductListBloc extends Bloc<ProductListEvent, ProductListState> {
  ProductListBloc({@required this.api});

  final ProductApi api;

  @override
  ProductListState get initialState => EmptyProductListState();

  @override
  Stream<ProductListState> mapEventToState(
    ProductListEvent event,
  ) async* {
    final prevState = currentState;

    if (event is FetchProductList) {
      yield* _fetch();
    }

    if (prevState is PopulatedProductListState ||
        prevState is EmptyProductListState) {
      if (event is AddProduct) {
        yield* _add(prevState, event);
      } else if (event is RemoveProduct) {
        yield* _remove(prevState, event);
      }
    }
  }

  Stream<ProductListState> _fetch() async* {
    yield LoadingProductListState();

    try {
      final res = await api.getAllProducts();

      yield res.isEmpty
          ? EmptyProductListState()
          : PopulatedProductListState(res);
    } catch (e, st) {
      yield ErrorProductListState();
    }
  }

  Stream<ProductListState> _add(
      ProductListState previousState, AddProduct event) async* {
    yield LoadingProductListState();

    try {
      final res = await api.addProduct(event.product);

      if (!res) {
        yield previousState;
        return;
      } else {
        final products = previousState is PopulatedProductListState
            ? previousState.products
            : <Product>[];
        products.add(event.product);

        yield PopulatedProductListState(products);
      }
    } catch (e, st) {
      yield ErrorProductListState();
    }
  }

  Stream<ProductListState> _remove(
      ProductListState previousState, RemoveProduct event) async* {
    yield LoadingProductListState();

    try {
      final res = await api.removeProduct(event.product.id);

      if (!res) {
        yield previousState;
        return;
      } else {
        final products = previousState is PopulatedProductListState
            ? previousState.products
            : [];

        products.remove(event.product);

        yield PopulatedProductListState(products);
      }
    } catch (e, st) {
      yield ErrorProductListState();
    }
  }
}
