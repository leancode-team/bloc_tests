import 'package:bloc_tests/models/product.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ProductListState {}

class EmptyProductListState extends ProductListState {}

class LoadingProductListState extends ProductListState {}

class PopulatedProductListState extends ProductListState {
  PopulatedProductListState(this.products);

  final List<Product> products;
}

class ErrorProductListState extends ProductListState {}
