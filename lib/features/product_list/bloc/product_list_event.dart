import 'package:bloc_tests/models/product.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ProductListEvent {}

class FetchProductList extends ProductListEvent {}

class AddProduct extends ProductListEvent {
  AddProduct(this.product);

  final Product product;
}

class RemoveProduct extends ProductListEvent {
  RemoveProduct(this.product);

  final Product product;
}
