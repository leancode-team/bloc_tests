import 'package:meta/meta.dart';

@immutable
abstract class SignUpFormEvent {}

class SignUp extends SignUpFormEvent {}

class UsernameChanged extends SignUpFormEvent {
  UsernameChanged(this.value);

  final String value;
}

class PasswordChanged extends SignUpFormEvent {
  PasswordChanged(this.value);

  final String value;
}
