import 'package:meta/meta.dart';

@immutable
abstract class SignUpFormState {}

class EditingSignUpFormState extends SignUpFormState {
  EditingSignUpFormState({
    this.username,
    this.password,
    this.usernameError,
    this.passwordError,
  });

  EditingSignUpFormState copyWith({
    String username,
    String password,
    bool usernameError,
    bool passwordError,
  }) {
    return EditingSignUpFormState(
      username: username ?? this.username,
      password: password ?? this.password,
      usernameError: usernameError ?? this.usernameError,
      passwordError: passwordError ?? this.passwordError,
    );
  }

  final String username;
  final String password;
  final bool usernameError;
  final bool passwordError;
}

class LoadingSignUpFormState extends SignUpFormState {}

class SignedUpState extends SignUpFormState {}
