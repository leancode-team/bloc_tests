import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:bloc_tests/api/user_api.dart';
import './bloc.dart';

class SignUpFormBloc extends Bloc<SignUpFormEvent, SignUpFormState> {
  SignUpFormBloc(this.api);

  final UserApi api;

  @override
  SignUpFormState get initialState => EditingSignUpFormState(
        username: '',
        password: '',
        usernameError: false,
        passwordError: false,
      );

  @override
  Stream<SignUpFormState> mapEventToState(
    SignUpFormEvent event,
  ) async* {
    final prevState = currentState;
    if (prevState is EditingSignUpFormState) {
      if (event is UsernameChanged) {
        yield prevState.copyWith(
          username: event.value,
          usernameError: event.value == '',
        );
      } else if (event is PasswordChanged) {
        yield prevState.copyWith(
          password: event.value,
          passwordError: event.value == '',
        );
      } else if (event is SignUp) {
        if (!_validate(prevState)) {
          yield prevState.copyWith(
            usernameError: prevState.username == '',
            passwordError: prevState.password == '',
          );
          return;
        }

        yield LoadingSignUpFormState();

        final res = await api.signUp(prevState.username, prevState.password);

        yield res ? SignedUpState() : prevState;
      }
    }
  }

  _validate(EditingSignUpFormState state) {
    return state.username != '' && state.password != '';
  }
}
