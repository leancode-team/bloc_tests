import 'package:bloc_tests/api/user_api.dart';
import 'package:bloc_tests/features/sign_up/bloc/bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class FakeUserApi extends Mock implements UserApi {}

void main() {
  group('SignUpFormBloc with initial state', () {
    SignUpFormBloc bloc;
    UserApi api;

    setUp(() {
      api = FakeUserApi();
      bloc = SignUpFormBloc(api);
    });

    tearDown(() {
      bloc.dispose();
    });

    test('Initial state is correct', () {
      expect(bloc.initialState, _isAnEmptyState);
    });

    test('Changing username should alter state', () {
      expectLater(
        bloc.state,
        emitsInOrder([
          _isAnEmptyState,
          _isAStateHaving(username: 'test', password: ''),
        ]),
      );

      bloc.dispatch(UsernameChanged('test'));
    });

    test('Changing password should alter state', () {
      expectLater(
        bloc.state,
        emitsInOrder([
          _isAnEmptyState,
          _isAStateHaving(username: '', password: 'test'),
        ]),
      );

      bloc.dispatch(PasswordChanged('test'));
    });

    test('Signup with errors shouldn\'t make an api call', () {
      bloc.dispatch(SignUp());

      verifyZeroInteractions(api);
    });

    test('Signup with empty fields should set errors', () {
      expectLater(
        bloc.state,
        emitsInOrder([
          _isAnEmptyState,
          _isAStateHaving(
              username: '',
              password: '',
              usernameError: true,
              passwordError: true),
        ]),
      );

      bloc.dispatch(SignUp());
    });
  });

  group('SignUpFormBloc with filled fields', () {
    SignUpFormBloc bloc;
    UserApi api;

    setUp(() async {
      api = FakeUserApi();
      bloc = SignUpFormBloc(api);

      bloc.dispatch(UsernameChanged('test'));
      bloc.dispatch(PasswordChanged('test'));

      await bloc.state.firstWhere((x) =>
          _isAStateHaving(username: 'test', password: 'test').matches(x, {}));
    });

    tearDown(() {
      bloc.dispose();
    });

    test('Changing username to empty string should set an error flag', () {
      expectLater(
        bloc.state,
        emitsInOrder([
          _isAStateHaving(username: 'test', password: 'test'),
          _isAStateHaving(username: '', password: 'test', usernameError: true),
        ]),
      );

      bloc.dispatch(UsernameChanged(''));
    });

    test('Changing password to empty string should set an error flag', () {
      expectLater(
        bloc.state,
        emitsInOrder([
          _isAStateHaving(username: 'test', password: 'test'),
          _isAStateHaving(username: 'test', password: '', passwordError: true),
        ]),
      );

      bloc.dispatch(PasswordChanged(''));
    });

    test('Signup with correct data makes an api call', () {
      expectLater(
        bloc.state,
        emitsInOrder([
          _isAStateHaving(username: 'test', password: 'test'),
          isA<LoadingSignUpFormState>(),
          isA<SignedUpState>(),
        ]),
      );

      when(api.signUp(any, any)).thenAnswer((_) async => true);

      bloc.dispatch(SignUp());
    });
  });
}

Matcher _isAStateHaving({
  String username,
  String password,
  bool usernameError = false,
  bool passwordError = false,
}) {
  return isA<EditingSignUpFormState>()
      .having((x) => x.username, 'username', username)
      .having((x) => x.password, 'password', password)
      .having((x) => x.usernameError, 'username error', usernameError)
      .having((x) => x.passwordError, 'password error', passwordError);
}

final _isAnEmptyState = _isAStateHaving(username: '', password: '');
