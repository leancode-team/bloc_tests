import 'package:bloc_tests/api/product_api.dart';
import 'package:bloc_tests/features/product_list/bloc/bloc.dart';
import 'package:bloc_tests/models/product.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class FakeProductApi extends Mock implements ProductApi {}

void main() {
  group('ProductListBloc tests with initial empty state', () {
    ProductListBloc bloc;
    ProductApi api;

    setUp(() {
      api = FakeProductApi();
      bloc = ProductListBloc(api: api);
    });

    tearDown(() {
      bloc.dispose();
    });

    test('Initial state is empty', () {
      expect(bloc.initialState, isA<EmptyProductListState>());
    });

    test('Fetch event updates state to populated if there are any products',
        () {
      expectLater(
        bloc.state,
        emitsInOrder([
          isA<EmptyProductListState>(),
          isA<LoadingProductListState>(),
          isA<PopulatedProductListState>(),
        ]),
      );

      when(api.getAllProducts()).thenAnswer((_) async => [_sampleProduct]);

      bloc.dispatch(FetchProductList());
    });

    test('Fetch event updates state to empty if there are no products', () {
      expectLater(
        bloc.state,
        emitsInOrder([
          isA<EmptyProductListState>(),
          isA<LoadingProductListState>(),
          isA<EmptyProductListState>(),
        ]),
      );

      when(api.getAllProducts()).thenAnswer((_) async => []);

      bloc.dispatch(FetchProductList());
    });

    test('Fetch event updates state to error if error is thrown', () {
      expectLater(
        bloc.state,
        emitsInOrder([
          isA<EmptyProductListState>(),
          isA<LoadingProductListState>(),
          isA<ErrorProductListState>(),
        ]),
      );

      when(api.getAllProducts()).thenThrow(Error());

      bloc.dispatch(FetchProductList());
    });

    test('Add event updates list with the new product', () {
      expectLater(
        bloc.state,
        emitsInOrder([
          isA<EmptyProductListState>(),
          isA<LoadingProductListState>(),
          isA<PopulatedProductListState>()
              .having((x) => x.products, 'products', hasLength(1)),
        ]),
      );

      when(api.addProduct(any)).thenAnswer((_) async => true);

      bloc.dispatch(AddProduct(_sampleProduct));
    });

    test('Add event doesn\'t change list if api returns false', () {
      expectLater(
        bloc.state,
        emitsInOrder([
          isA<EmptyProductListState>(),
          isA<LoadingProductListState>(),
          isA<EmptyProductListState>(),
        ]),
      );

      when(api.addProduct(any)).thenAnswer((_) async => false);

      bloc.dispatch(AddProduct(_sampleProduct));
    });

    test('Add event updates state to error if error is thrown', () {
      expectLater(
        bloc.state,
        emitsInOrder([
          isA<EmptyProductListState>(),
          isA<LoadingProductListState>(),
          isA<ErrorProductListState>(),
        ]),
      );

      when(api.addProduct(any)).thenThrow(Error());

      bloc.dispatch(AddProduct(_sampleProduct));
    });
  });

  group('ProductListBloc tests with initial populated state', () {
    ProductListBloc bloc;
    ProductApi api;

    setUp(() async {
      api = FakeProductApi();
      bloc = ProductListBloc(api: api);

      when(api.getAllProducts()).thenAnswer((_) async => [_sampleProduct]);
      bloc.dispatch(FetchProductList());

      await bloc.state.firstWhere((x) => x is PopulatedProductListState);
    });

    tearDown(() {
      bloc.dispose();
    });

    test('Remove event updates list with the new product', () {
      expectLater(
        bloc.state,
        emitsInOrder([
          isA<PopulatedProductListState>()
              .having((x) => x.products, 'products', hasLength(1)),
          isA<LoadingProductListState>(),
          isA<PopulatedProductListState>()
              .having((x) => x.products, 'products', hasLength(0)),
        ]),
      );

      when(api.removeProduct(any)).thenAnswer((_) async => true);

      bloc.dispatch(RemoveProduct(_sampleProduct));
    });

    test('Remove event doesn\'t change list if api returns false', () {
      expectLater(
        bloc.state,
        emitsInOrder([
          isA<PopulatedProductListState>()
              .having((x) => x.products, 'products', hasLength(1)),
          isA<LoadingProductListState>(),
          isA<PopulatedProductListState>()
              .having((x) => x.products, 'products', hasLength(1)),
        ]),
      );

      when(api.removeProduct(any)).thenAnswer((_) async => false);

      bloc.dispatch(RemoveProduct(_sampleProduct));
    });

    test('Remove event updates state to error if error is thrown', () {
      expectLater(
        bloc.state,
        emitsInOrder([
          isA<PopulatedProductListState>(),
          isA<LoadingProductListState>(),
          isA<ErrorProductListState>(),
        ]),
      );

      when(api.removeProduct(any)).thenThrow(Error());

      bloc.dispatch(RemoveProduct(_sampleProduct));
    });
  });
}

final _sampleProduct = Product()
  ..id = '00000000-0000-0000-0000- 000000000000'
  ..name = 'test'
  ..description = 'testDesc'
  ..valueInSmallestUnit = 100;
